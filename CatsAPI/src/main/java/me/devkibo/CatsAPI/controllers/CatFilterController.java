package me.devkibo.CatsAPI.controllers;

import me.devkibo.CatsAPI.dto.cat.GetCatDto;
import me.devkibo.CatsAPI.models.Cat;
import me.devkibo.CatsAPI.services.CatService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/cats/filter")
public class CatFilterController {
    @Autowired
    private CatService catService;
    @Autowired
    private ModelMapper modelMapper;

    private GetCatDto convertToDto(Cat cat) {
        return modelMapper.map(cat, GetCatDto.class);
    }
    // GET /breed/{breed}
    @GetMapping("/breed/{breed}")
    public ResponseEntity<List<GetCatDto>> getAllByBreed(@PathVariable String breed) {
        List<GetCatDto> res = catService.getAll().stream().filter(dog -> dog.getBreed().getName().toLowerCase().equals(breed.toLowerCase())).map(this::convertToDto).collect(Collectors.toList());

        if (res.size() == 0) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(res, HttpStatus.OK);
    }
    // GET /gender/{gender}
    @GetMapping("/gender/{gender}")
    public ResponseEntity<List<GetCatDto>> getAllByGender(@PathVariable String gender) {
        List<GetCatDto> res = catService.getAll().stream().filter(dog -> dog.getGender().toString().toLowerCase().equals(gender.toLowerCase())).map(this::convertToDto).collect(Collectors.toList());

        if (res.size() == 0) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(res, HttpStatus.OK);
    }
    // GET /tag/{tag}
    @GetMapping("/tag/{tag}")
    public ResponseEntity<List<GetCatDto>> getAllByTag(@PathVariable String tag) {
        List<GetCatDto> res = catService.getAll().stream().filter(dog -> dog.getTag().toLowerCase().contains(tag.toLowerCase())).map(this::convertToDto).collect(Collectors.toList());

        if (res.size() == 0) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(res, HttpStatus.OK);
    }

}
