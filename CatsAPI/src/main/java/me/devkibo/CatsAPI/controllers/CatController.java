package me.devkibo.CatsAPI.controllers;

import me.devkibo.CatsAPI.dto.cat.CreateCatDto;
import me.devkibo.CatsAPI.dto.cat.GetCatDto;
import me.devkibo.CatsAPI.dto.cat.UpdateCatDto;
import me.devkibo.CatsAPI.models.Cat;
import me.devkibo.CatsAPI.models.Vaccine;
import me.devkibo.CatsAPI.services.BreedService;
import me.devkibo.CatsAPI.services.CatService;
import me.devkibo.CatsAPI.services.VaccineService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api")
public class CatController {

    @Autowired
    private CatService catService;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private BreedService breedService;
    @Autowired
    private VaccineService vaccineService;

    private GetCatDto convertToDto(Cat cat) {
        return modelMapper.map(cat, GetCatDto.class);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
    // GET api/cats
    @GetMapping("/cats")
    public ResponseEntity<List<GetCatDto>> getAll() {
        List<GetCatDto> results = catService.getAll().stream().map(this::convertToDto).collect(Collectors.toList());

        return new ResponseEntity<>(results, HttpStatus.OK);
    }
    // GET api/cats/{id}
    @GetMapping("/cats/{id}")
    public ResponseEntity<GetCatDto> getById(@PathVariable int id) {
        Cat d = catService.getById(id);
        if (d == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        GetCatDto result = modelMapper.map(d, GetCatDto.class);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }
    // POST api/cats
    @PostMapping("/cats")
    public ResponseEntity<GetCatDto> create(@Valid @RequestBody CreateCatDto dog) {

        List<Cat> foundCat = catService.getAll().stream().filter(d -> d.getTag().toLowerCase().equals(dog.getTag().toLowerCase()) && !d.isDeleted()).collect(Collectors.toList());

        Map<String, String> error = new HashMap<>();

        error.put("error", "Tag already exists.");

        if (foundCat.size() > 0) {
            return new ResponseEntity(error,HttpStatus.BAD_REQUEST);
        }

        Set<Vaccine> vacc = new HashSet<>();
        for(int i : dog.getVaccines()) {
            vacc.add(vaccineService.getById(i));
        }
        Cat d = modelMapper.map(dog, Cat.class);
        d.setVaccines(vacc);
        d.setBreed(breedService.getById(dog.getBreed()));

        catService.create(d);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(d.getId())
                .toUri();

        return ResponseEntity.created(location).body(modelMapper.map(d, GetCatDto.class));
    }
    // PUT api/cats/{id}
    @PutMapping("/cats/{id}")
    public ResponseEntity update(@PathVariable int id,@Valid @RequestBody UpdateCatDto dog) {

        Cat d = catService.getById(id);

        if (d == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        Set<Vaccine> vacc = new HashSet<>();
        for(int i : dog.getVaccines()) {
            vacc.add(vaccineService.getById(i));
        }
        Cat oldCat = modelMapper.map(dog, Cat.class);
        oldCat.setVaccines(vacc);
        oldCat.setBreed(breedService.getById(dog.getBreed()));

        catService.update(id, oldCat);

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
    // DELETE /api/cats/{id}
    @DeleteMapping("/cats/{id}")
    public ResponseEntity delete(@PathVariable int id) {
        Cat d = catService.getById(id);
        if (d == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        catService.delete(d);

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
