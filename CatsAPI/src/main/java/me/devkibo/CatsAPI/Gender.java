package me.devkibo.CatsAPI;

public enum Gender {
    MALE, FEMALE;

    @Override
    public String toString() {
        String res = "";

        switch (this) {
            case MALE:
                res = "male";
                break;
            case FEMALE:
                res = "female";
                break;
            default:
                res = "";
                break;
        }

        return res;
    }
}
