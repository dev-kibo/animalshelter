package me.devkibo.CatsAPI.services;

import me.devkibo.CatsAPI.models.Cat;
import me.devkibo.CatsAPI.repositories.CatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CatService {
    @Autowired
    private CatRepository catRepository;

    public List<Cat> getAll() {
        return catRepository.findAll().stream().filter(dog -> !dog.isDeleted()).collect(Collectors.toList());
    }

    public Cat getById(int id) {
        Optional<Cat> d = catRepository.findById(id);
        if (d.isPresent() && d.get().isDeleted()) {
            return null;
        }
        return d.isPresent() ? d.get() : null;
    }

    public void create(Cat cat) {
        catRepository.save(cat);
    }

    public void update(int id, Cat cat) {
        catRepository.findById(id).map(d -> {
            d.setWeight(cat.getWeight());
            d.setBreed(cat.getBreed());
            d.setVaccines(cat.getVaccines());
            d.setGender(cat.getGender());
            d.setPreviousOwner(cat.getPreviousOwner());
            d.setDateOfBirth(cat.getDateOfBirth());
           return catRepository.save(d);
        });
    }

    public void delete(Cat cat) {
        cat.setDeleted(true);
        catRepository.save(cat);
    }

}
