package me.devkibo.CatsAPI.models;

import me.devkibo.CatsAPI.Gender;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "cat")
public class Cat {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(nullable = false)
    private String tag;
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Gender gender;
    @ManyToOne
    @JoinColumn(name = "breed_id")
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    private Breed breed;
    @Column(precision = 2, nullable = false)
    private double weight;
    private String dateOfBirth;
    private boolean previousOwner;
    @ColumnDefault(value = "false")
    private boolean isDeleted;
    @ManyToMany
    @JoinTable(name = "cat_vaccine", joinColumns = {@JoinColumn(name = "cat_id")}, inverseJoinColumns = {@JoinColumn(name = "vaccine_id")})
    Set<Vaccine> vaccines;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Breed getBreed() {
        return breed;
    }

    public void setBreed(Breed breed) {
        this.breed = breed;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public boolean getPreviousOwner() {
        return previousOwner;
    }

    public void setPreviousOwner(boolean previousOwner) {
        this.previousOwner = previousOwner;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public Set<Vaccine> getVaccines() {
        return vaccines;
    }

    public void setVaccines(Set<Vaccine> vaccines) {
        this.vaccines = vaccines;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }
}
