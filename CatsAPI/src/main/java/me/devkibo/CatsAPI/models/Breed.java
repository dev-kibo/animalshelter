package me.devkibo.CatsAPI.models;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "breed")
public class Breed {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(unique = true, nullable = false)
    private String name;
    @OneToMany(mappedBy = "breed")
    @JsonBackReference
    private Set<Cat> cats;

    public Breed() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Cat> getCats() {
        return cats;
    }

    public void setCats(Set<Cat> cats) {
        this.cats = cats;
    }

    public Breed(String name, Set<Cat> cats) {
        this.name = name;
        this.cats = cats;
    }

    public Breed(int id, String name, Set<Cat> cats) {
        this.id = id;
        this.name = name;
        this.cats = cats;
    }
}
