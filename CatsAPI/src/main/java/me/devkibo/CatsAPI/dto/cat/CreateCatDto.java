package me.devkibo.CatsAPI.dto.cat;

import me.devkibo.CatsAPI.Gender;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

public class CreateCatDto {

    @NotBlank(message = "Tag can't be empty")
    private String tag;
    @Min(value = 1, message = "Breed can't be empty")
    private int breed;
    @Min(value = 1, message = "Weight can't be empty")
    private double weight;
    private String dateOfBirth;
    @NotNull(message = "Previous owner can't be empty")
    private Boolean previousOwner;
    @NotNull(message = "Gender can't be empty")
    private Gender gender;
    List<Integer> vaccines;

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public int getBreed() {
        return breed;
    }

    public void setBreed(int breed) {
        this.breed = breed;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public boolean getPreviousOwner() {
        return previousOwner;
    }

    public void setPreviousOwner(boolean previousOwner) {
        this.previousOwner = previousOwner;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = Enum.valueOf(Gender.class,gender.toUpperCase());
    }

    public List<Integer> getVaccines() {
        return vaccines;
    }

    public void setVaccines(List<Integer> vaccines) {
        this.vaccines = vaccines;
    }
}
