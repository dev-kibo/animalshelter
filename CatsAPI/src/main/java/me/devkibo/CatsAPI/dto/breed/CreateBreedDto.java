package me.devkibo.CatsAPI.dto.breed;

import javax.validation.constraints.NotNull;

public class CreateBreedDto {
    @NotNull(message = "Name is required")
    private String name;

    public CreateBreedDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CreateBreedDto(String name) {
        this.name = name;
    }
}
