package me.devkibo.CatsAPI.dto.vaccine;

import javax.validation.constraints.NotNull;

public class CreateVaccineDto {
    @NotNull(message = "Name is required")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CreateVaccineDto() {}

    public CreateVaccineDto(String name) {
        this.name = name;
    }
}
