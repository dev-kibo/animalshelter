package me.devkibo.DogsAPI.dto.dog;

import me.devkibo.DogsAPI.Gender;
import me.devkibo.DogsAPI.dto.breed.GetBreedDto;
import me.devkibo.DogsAPI.dto.vaccine.GetVaccineDto;

import java.util.Set;

public class GetDogDto {
    private int id;
    private String tag;
    private GetBreedDto breed;
    private double weight;
    private String dateOfBirth;
    private boolean previousOwner;
    private Gender gender;
    private Set<GetVaccineDto> vaccines;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public GetBreedDto getBreed() {
        return breed;
    }

    public void setBreed(GetBreedDto breed) {
        this.breed = breed;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public boolean isPreviousOwner() {
        return previousOwner;
    }

    public void setPreviousOwner(boolean previousOwner) {
        this.previousOwner = previousOwner;
    }

    public String getGender() {
        return gender.toString();
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Set<GetVaccineDto> getVaccines() {
        return vaccines;
    }

    public void setVaccines(Set<GetVaccineDto> vaccines) {
        this.vaccines = vaccines;
    }
}
