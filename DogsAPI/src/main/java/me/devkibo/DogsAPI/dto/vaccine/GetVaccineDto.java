package me.devkibo.DogsAPI.dto.vaccine;

public class GetVaccineDto {
    private int id;
    private String name;

    public GetVaccineDto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
