package me.devkibo.DogsAPI.models;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "breed")
public class Breed {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(unique = true, nullable = false)
    private String name;

    @OneToMany(mappedBy = "breed")
    @JsonBackReference
    private Set<Dog> dogs;

    public Breed() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Dog> getDogs() {
        return dogs;
    }

    public void setDogs(Set<Dog> dogs) {
        this.dogs = dogs;
    }

    public Breed(String name, Set<Dog> dogs) {
        this.name = name;
        this.dogs = dogs;
    }

    public Breed(int id, String name, Set<Dog> dogs) {
        this.id = id;
        this.name = name;
        this.dogs = dogs;
    }
}
