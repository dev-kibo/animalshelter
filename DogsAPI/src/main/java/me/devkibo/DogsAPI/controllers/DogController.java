package me.devkibo.DogsAPI.controllers;

import me.devkibo.DogsAPI.dto.dog.CreateDogDto;
import me.devkibo.DogsAPI.dto.dog.GetDogDto;
import me.devkibo.DogsAPI.dto.dog.UpdateDogDto;
import me.devkibo.DogsAPI.models.Dog;
import me.devkibo.DogsAPI.models.Vaccine;
import me.devkibo.DogsAPI.services.BreedService;
import me.devkibo.DogsAPI.services.DogService;
import me.devkibo.DogsAPI.services.VaccineService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api")
public class DogController {

    @Autowired
    private DogService dogService;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private BreedService breedService;
    @Autowired
    private VaccineService vaccineService;

    private GetDogDto convertToDto(Dog dog) {
        return modelMapper.map(dog, GetDogDto.class);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

    // GET api/dogs
    @GetMapping("/dogs")
    public ResponseEntity<List<GetDogDto>> getAll() {
        List<GetDogDto> results = dogService.getAll().stream().map(this::convertToDto).collect(Collectors.toList());

        return new ResponseEntity<>(results, HttpStatus.OK);
    }

    // GET api/dogs/{id}
    @GetMapping("/dogs/{id}")
    public ResponseEntity<GetDogDto> getById(@PathVariable int id) {
        Dog d = dogService.getById(id);
        if (d == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        GetDogDto result = modelMapper.map(d, GetDogDto.class);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    // POST api/dogs
    @PostMapping("/dogs")
    public ResponseEntity<GetDogDto> create(@Valid @RequestBody CreateDogDto dog) {

        List<Dog> foundDog = dogService.getAll().stream().filter(d -> d.getTag().toLowerCase().equals(dog.getTag().toLowerCase()) && !d.isDeleted()).collect(Collectors.toList());

        Map<String, String> error = new HashMap<>();

        error.put("error", "Tag already exists.");


        if (foundDog.size() > 0) {
            return new ResponseEntity(error, HttpStatus.BAD_REQUEST);
        }

        Set<Vaccine> vacc = new HashSet<>();
        for (int i : dog.getVaccines()) {
            vacc.add(vaccineService.getById(i));
        }
        Dog d = modelMapper.map(dog, Dog.class);
        d.setVaccines(vacc);
        d.setBreed(breedService.getById(dog.getBreed()));

        dogService.create(d);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(d.getId())
                .toUri();

        return ResponseEntity.created(location).body(modelMapper.map(d, GetDogDto.class));
    }

    // PUT api/dogs/{id}
    @PutMapping("/dogs/{id}")
    public ResponseEntity update(@PathVariable int id, @Valid @RequestBody UpdateDogDto dog) {

        Dog d = dogService.getById(id);

        if (d == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        Set<Vaccine> vacc = new HashSet<>();
        for (int i : dog.getVaccines()) {
            vacc.add(vaccineService.getById(i));
        }
        Dog oldDog = modelMapper.map(dog, Dog.class);
        oldDog.setVaccines(vacc);
        oldDog.setBreed(breedService.getById(dog.getBreed()));

        dogService.update(id, oldDog);

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    // DELETE /api/dogs/{id}
    @DeleteMapping("/dogs/{id}")
    public ResponseEntity delete(@PathVariable int id) {
        Dog d = dogService.getById(id);
        if (d == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        dogService.delete(d);

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
