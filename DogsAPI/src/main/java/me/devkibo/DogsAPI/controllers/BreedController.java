package me.devkibo.DogsAPI.controllers;

import me.devkibo.DogsAPI.dto.breed.CreateBreedDto;
import me.devkibo.DogsAPI.dto.breed.GetBreedDto;
import me.devkibo.DogsAPI.models.Breed;
import me.devkibo.DogsAPI.services.BreedService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/dogs")
public class BreedController {
    @Autowired
    private BreedService breedService;
    @Autowired
    private ModelMapper modelMapper;

    private GetBreedDto convertToDto(Breed breed) {
        return modelMapper.map(breed, GetBreedDto.class);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

    // GET /breeds
    @GetMapping("/breeds")
    public ResponseEntity<List<GetBreedDto>> getAll() {
        List<GetBreedDto> results = breedService.getAll().stream().map(this::convertToDto).collect(Collectors.toList());

        return new ResponseEntity<>(results, HttpStatus.OK);
    }

    // GET /breeds/{id}
    @GetMapping("/breeds/{id}")
    public ResponseEntity<GetBreedDto> getById(@PathVariable int id) {
        Breed b = breedService.getById(id);

        if (b == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        GetBreedDto breed = modelMapper.map(b, GetBreedDto.class);

        return new ResponseEntity<>(breed, HttpStatus.OK);
    }

    // POST /breeds
    @PostMapping("/breeds")
    public ResponseEntity<GetBreedDto> create(@Valid @RequestBody CreateBreedDto breed) {

        Breed b = modelMapper.map(breed, Breed.class);

        breedService.create(b);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(b.getId())
                .toUri();

        return ResponseEntity.created(location).body(modelMapper.map(b, GetBreedDto.class));
    }

    // DELETE /breeds/{id}
    @DeleteMapping("/breeds/{id}")
    public ResponseEntity delete(@PathVariable int id) {
        Breed breed = breedService.getById(id);

        if (breed == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        breedService.delete(breed);

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

}
