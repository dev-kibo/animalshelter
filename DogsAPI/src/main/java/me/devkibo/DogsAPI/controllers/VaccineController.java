package me.devkibo.DogsAPI.controllers;

import me.devkibo.DogsAPI.dto.vaccine.CreateVaccineDto;
import me.devkibo.DogsAPI.dto.vaccine.GetVaccineDto;
import me.devkibo.DogsAPI.models.Vaccine;
import me.devkibo.DogsAPI.services.VaccineService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/dogs")
public class VaccineController {
    @Autowired
    private VaccineService vaccineService;
    @Autowired
    private ModelMapper modelMapper;

    private GetVaccineDto convertToDto(Vaccine vaccine) {
        return modelMapper.map(vaccine, GetVaccineDto.class);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

    // GET /vaccines
    @GetMapping("/vaccines")
    public ResponseEntity<List<GetVaccineDto>> get() {
        List<GetVaccineDto> res = vaccineService.getAll().stream().map(this::convertToDto).collect(Collectors.toList());

        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    //GET /vaccines/{id}
    @GetMapping("/vaccines/{id}")
    public ResponseEntity<GetVaccineDto> getById(@PathVariable int id) {
        Vaccine v = vaccineService.getById(id);
        if (v == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(modelMapper.map(v, GetVaccineDto.class), HttpStatus.OK);
    }

    // POST /vaccines
    @PostMapping("/vaccines")
    public ResponseEntity create(@Valid @RequestBody CreateVaccineDto vaccine) {
        Vaccine v = modelMapper.map(vaccine, Vaccine.class);

        vaccineService.create(v);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(v.getId())
                .toUri();

        return ResponseEntity.created(location).body(modelMapper.map(v, GetVaccineDto.class));
    }

    // DELETE /vaccines/{id}
    @DeleteMapping("/vaccines/{id}")
    public ResponseEntity delete(@PathVariable int id) {
        Vaccine v = vaccineService.getById(id);
        if (v == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        vaccineService.delete(v);

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
