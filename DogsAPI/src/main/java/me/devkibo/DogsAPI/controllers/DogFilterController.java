package me.devkibo.DogsAPI.controllers;

import me.devkibo.DogsAPI.dto.dog.GetDogDto;
import me.devkibo.DogsAPI.models.Dog;
import me.devkibo.DogsAPI.services.DogService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/dogs/filter")
public class DogFilterController {
    @Autowired
    private DogService dogService;
    @Autowired
    private ModelMapper modelMapper;

    private GetDogDto convertToDto(Dog dog) {
        return modelMapper.map(dog, GetDogDto.class);
    }

    // GET /breed/{breed}
    @GetMapping("/breed/{breed}")
    public ResponseEntity<List<GetDogDto>> getAllByBreed(@PathVariable String breed) {
        List<GetDogDto> res = dogService.getAll().stream().filter(dog -> dog.getBreed().getName().toLowerCase().equals(breed.toLowerCase())).map(this::convertToDto).collect(Collectors.toList());

        if (res.size() == 0) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    // GET /gender/{gender}
    @GetMapping("/gender/{gender}")
    public ResponseEntity<List<GetDogDto>> getAllByGender(@PathVariable String gender) {
        List<GetDogDto> res = dogService.getAll().stream().filter(dog -> dog.getGender().toString().toLowerCase().equals(gender.toLowerCase())).map(this::convertToDto).collect(Collectors.toList());

        if (res.size() == 0) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    // GET /tag/{tag}
    @GetMapping("/tag/{tag}")
    public ResponseEntity<List<GetDogDto>> getAllByTag(@PathVariable String tag) {
        List<GetDogDto> res = dogService.getAll().stream().filter(dog -> dog.getTag().toLowerCase().contains(tag.toLowerCase())).map(this::convertToDto).collect(Collectors.toList());

        if (res.size() == 0) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(res, HttpStatus.OK);
    }

}
