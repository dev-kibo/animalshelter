package me.devkibo.DogsAPI.services;

import me.devkibo.DogsAPI.models.Vaccine;
import me.devkibo.DogsAPI.repositories.VaccineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class VaccineService {
    @Autowired
    private VaccineRepository vaccineRepository;

    public List<Vaccine> getAll() {
        return vaccineRepository.findAll();
    }

    public Vaccine getById(int id) {
        Optional<Vaccine> res = vaccineRepository.findById(id);

        if (res.isPresent()) {
            return res.get();
        }

        return null;
    }

    public void create(Vaccine vaccine) {
        vaccineRepository.save(vaccine);
    }

    public void delete(Vaccine vaccine) {
        vaccineRepository.delete(vaccine);
    }
}
