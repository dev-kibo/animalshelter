package me.devkibo.DogsAPI.services;

import me.devkibo.DogsAPI.models.Breed;
import me.devkibo.DogsAPI.repositories.BreedRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BreedService {
    @Autowired
    private BreedRepository breedRepository;

    public List<Breed> getAll() {
        return breedRepository.findAll();
    }

    public Breed getById(int id) {
        Optional<Breed> res = breedRepository.findById(id);

        if (res.isPresent()) {
            return res.get();
        }

        return null;
    }

    public void create(Breed breed) {
        breedRepository.save(breed);
    }

    public void delete(Breed breed) {
        breedRepository.delete(breed);
    }

}
