package me.devkibo.DogsAPI.services;

import me.devkibo.DogsAPI.models.Dog;
import me.devkibo.DogsAPI.repositories.DogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DogService {
    @Autowired
    private DogRepository dogRepository;

    public List<Dog> getAll() {
        return dogRepository.findAll().stream().filter(dog -> !dog.isDeleted()).collect(Collectors.toList());
    }

    public Dog getById(int id) {
        Optional<Dog> d = dogRepository.findById(id);
        if (d.isPresent() && d.get().isDeleted()) {
            return null;
        }
        return d.isPresent() ? d.get() : null;
    }

    public void create(Dog dog) {
        dogRepository.save(dog);
    }

    public void update(int id, Dog dog) {
        dogRepository.findById(id).map(d -> {
            d.setWeight(dog.getWeight());
            d.setBreed(dog.getBreed());
            d.setVaccines(dog.getVaccines());
            d.setGender(dog.getGender());
            d.setPreviousOwner(dog.getPreviousOwner());
            d.setDateOfBirth(dog.getDateOfBirth());
            return dogRepository.save(d);
        });
    }

    public void delete(Dog dog) {
        dog.setDeleted(true);
        dogRepository.save(dog);
    }

}
