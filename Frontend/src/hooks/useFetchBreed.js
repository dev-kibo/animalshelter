import { useState, useEffect } from "react";
import axios from "axios";

export default function useFetchBreed(breed, type) {
  const [breedRes, setBreedRes] = useState([]);

  useEffect(() => {
    if (breed !== "all") {
      async function fetch() {
        try {
          let res;
          if (type === "dog") {
            res = await axios.get(
              `http://localhost:8080/api/dogs/filter/breed/${breed}`
            );
          } else if (type === "cat") {
            res = await axios.get(
              `http://localhost:8081/api/cats/filter/breed/${breed}`
            );
          } else {
            setBreedRes([]);
          }

          setBreedRes(res.data);
        } catch (e) {
          setBreedRes([]);
        }
      }
      fetch();
    }
  }, [breed, type]);

  return breedRes;
}
