import { useState, useEffect } from "react";
import axios from "axios";

export default function useFetchAllBreeds(type) {
  const [breeds, setBreeds] = useState([]);

  useEffect(() => {
    const fetch = async () => {
      try {
        let res;
        if (type === "dog") {
          res = await axios.get("http://localhost:8080/api/dogs/breeds");
        } else if (type === "cat") {
          res = await axios.get("http://localhost:8081/api/cats/breeds");
        } else {
          setBreeds([]);
        }
        setBreeds(res.data);
      } catch (e) {}
    };
    fetch();
  }, [type]);

  return breeds;
}
