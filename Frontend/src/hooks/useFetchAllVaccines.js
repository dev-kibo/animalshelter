import { useState, useEffect } from "react";
import axios from "axios";

export default function useFetchAllVaccines(type) {
  const [data, setData] = useState([]);

  useEffect(() => {
    const fetch = async () => {
      try {
        let res;
        if (type === "dog") {
          res = await axios.get("http://localhost:8080/api/dogs/vaccines");
        } else if (type === "cat") {
          res = await axios.get("http://localhost:8081/api/cats/vaccines");
        } else {
          setData([]);
        }
        setData(res.data);
      } catch (e) {
        setData([]);
      }
    };
    fetch();
  }, [type]);

  return data;
}
