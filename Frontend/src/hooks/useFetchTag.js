import { useState, useEffect } from "react";
import axios from "axios";

export default function useFetchTag(tag, type) {
  const [tagRes, setTagRes] = useState([]);

  useEffect(() => {
    async function fetch() {
      try {
        let res;
        if (tag.length > 0 && type === "dog") {
          res = await axios.get(
            `http://localhost:8080/api/dogs/filter/tag/${tag}`
          );
        } else if (tag.length > 0 && type === "cat") {
          res = await axios.get(
            `http://localhost:8081/api/cats/filter/tag/${tag}`
          );
        } else {
          setTagRes([]);
        }
        setTagRes(res.data);
      } catch (e) {
        setTagRes([]);
      }
    }
    fetch();
  }, [tag, type]);

  return tagRes;
}
