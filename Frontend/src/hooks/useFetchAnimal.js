import { useState, useEffect } from "react";
import axios from "axios";

export default function useFetchAnimal(id, pathname) {
  const [animal, setAnimal] = useState({ breed: {}, vaccines: [] });

  let type = "dog";

  if (pathname.includes("dog")) {
    type = "dog";
  } else if (pathname.includes("cat")) {
    type = "cat";
  }

  useEffect(() => {
    async function fetch() {
      try {
        let res;
        if (type === "dog") {
          res = await axios.get(`http://localhost:8080/api/dogs/${id}`);
        } else if (type === "cat") {
          res = await axios.get(`http://localhost:8081/api/cats/${id}`);
        }
        setAnimal(res.data);
      } catch (e) {
        setAnimal({});
      }
    }
    fetch();
  }, [id, type]);

  return animal;
}
