import { useEffect, useState } from "react";
import axios from "axios";

export default function useFetchAnimals(type) {
  const [data, setData] = useState([]);

  useEffect(() => {
    async function download() {
      try {
        let res;
        if (type === "dog") {
          res = await axios.get("http://localhost:8080/api/dogs");
        } else if (type === "cat") {
          res = await axios.get("http://localhost:8081/api/cats");
        } else {
          setData([]);
        }
        setData(res.data);
      } catch (e) {
        setData([]);
      }
    }
    download();
  }, [type]);

  return data;
}
