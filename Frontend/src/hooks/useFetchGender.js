import { useState, useEffect } from "react";
import axios from "axios";

export default function useFetchGender(gender, type) {
  const [results, setResults] = useState([]);

  useEffect(() => {
    async function fetch() {
      try {
        let res;
        if (gender !== "all" && type === "dog") {
          res = await axios.get(
            `http://localhost:8080/api/dogs/filter/gender/${gender}`
          );
        } else if (gender !== "all" && type === "cat") {
          res = await axios.get(
            `http://localhost:8081/api/cats/filter/gender/${gender}`
          );
        } else {
          setResults([]);
        }
        setResults(res.data);
      } catch (e) {
        setResults([]);
      }
    }
    fetch();
  }, [gender, type]);

  return results;
}
