import React from "react";
import Sidebar from "./components/Sidebar";
import { Switch, Route } from "react-router";
import Home from "./components/Home";
import AddPage from "./components/AddPage";
import RemovePage from "./components/RemovePage";
import UpdatePage from "./components/UpdatePage";
import AnimalPage from "./components/AnimalPage/AnimalPage";
import Manage from "./components/Manage/Manage";

function App() {
  return (
    <div className="w-screen h-screen flex">
      <Sidebar />
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route path="/add">
          <AddPage />
        </Route>
        <Route path="/remove">
          <RemovePage />
        </Route>
        <Route path="/update">
          <UpdatePage />
        </Route>
        <Route path="/animals/:type/:id">
          <AnimalPage />
        </Route>
        <Route path="/manage">
          <Manage />
        </Route>
      </Switch>
    </div>
  );
}

export default App;
