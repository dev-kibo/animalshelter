import React from "react";

export default function Notification({ text, color }) {
  return (
    <div
      className={`w-full h-12 flex items-center px-8 rounded-sm overflow-hidden bg-${color}-400 shadow-lg mb-3`}
    >
      <p className={`text-${color}-100`}>{text}</p>
    </div>
  );
}
