import React, { useState } from "react";
import Breed from "./Breed/Breed";
import Vaccine from "./Vaccine/Vaccine";
import AnimalType from "./AnimalType";
import Notification from "../Notification";

export default function Manage() {
  const [isValid, setIsValid] = useState(false);
  const [type, setType] = useState("");
  const [notif, setNotif] = useState("");
  const [notifColor, setNotifColor] = useState("");

  const handleIsValid = (e) => {
    const val = e.target.value;
    if (val === "cat" || val === "dog") {
      setIsValid(true);
      setType(val);
    } else {
      setIsValid(false);
      setType(val);
    }
  };

  return (
    <div className="w-full lg:p-12 xl:p-24 bg-gray-300">
      {notif ? <Notification color={notifColor} text={notif} /> : ""}
      <div className="rounded-sm flex flex-col overflow-hidden p-8 bg-gray-100 shadow-lg">
        <AnimalType handleIsValid={handleIsValid} />
        <div className="flex divide-x-2 divide-teal-500">
          <Breed
            type={type}
            setNotif={setNotif}
            setNotifColor={setNotifColor}
            isValid={isValid}
          />
          <Vaccine
            type={type}
            isValid={isValid}
            setNotifColor={setNotifColor}
            setNotif={setNotif}
          />
        </div>
      </div>
    </div>
  );
}
