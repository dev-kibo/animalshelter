import React, { useState } from "react";
import useFetchAllBreeds from "../../../hooks/useFetchAllBreeds";
import Add from "./Add";
import Remove from "./Remove";
import axios from "axios";

export default function Breed({ isValid, type, setNotif, setNotifColor }) {
  const [isInputValid, setIsInputValid] = useState(false);
  const [isSelectValid, setIsSelectValid] = useState(false);
  const [newBreed, setNewBreed] = useState("");
  const [breedToRemove, setBreedToRemove] = useState();

  const breeds = useFetchAllBreeds(type);

  const displayBreeds = breeds.map((b) => (
    <option key={b.id} value={b.id}>
      {b.name}
    </option>
  ));

  const handleAdd = async () => {
    try {
      const data = {
        name: newBreed,
      };
      if (type === "dog") {
        await axios.post("http://localhost:8080/api/dogs/breeds", data);
        setNotif("New breed added successfully.");
        setNotifColor("green");
      } else if (type === "cat") {
        await axios.post("http://localhost:8081/api/cats/breeds", data);
        setNotif("New breed added successfully.");
        setNotifColor("green");
      }
    } catch (e) {
      setNotif("Adding failed successfully.");
      setNotifColor("yellow");
    }
  };

  const handleRemove = async () => {
    try {
      if (type === "dog") {
        await axios.delete(
          `http://localhost:8080/api/dogs/breeds/${breedToRemove}`
        );
        setNotif("Breed removed successfully.");
        setNotifColor("green");
      } else if (type === "cat") {
        await axios.delete(
          `http://localhost:8081/api/cats/breeds/${breedToRemove}`
        );
        setNotif("Breed removed successfully.");
        setNotifColor("green");
      }
    } catch (e) {}
  };

  return (
    <div className="flex-1 flex flex-col p-4">
      <p className="text-teal-500 mb-4">Add or remove breed</p>
      <Add
        setIsInputValid={setIsInputValid}
        isValid={isValid}
        isInputValid={isInputValid}
        setNewBreed={setNewBreed}
        handleAdd={handleAdd}
      />
      <Remove
        setIsSelectValid={setIsSelectValid}
        isValid={isValid}
        displayBreeds={displayBreeds}
        isSelectValid={isSelectValid}
        handleRemove={handleRemove}
        setBreedToRemove={setBreedToRemove}
      />
    </div>
  );
}
