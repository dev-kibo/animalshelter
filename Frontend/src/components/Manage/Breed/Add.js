import React, { useState } from "react";

export default function Add({
  isValid,
  isInputValid,
  setIsInputValid,
  setNewBreed,
  handleAdd,
}) {
  const isBtnDisabled = useState(isValid && isInputValid);

  const handleInput = (e) => {
    if (e.target.value.length > 0) {
      setIsInputValid(true);
      setNewBreed(e.target.value);
    } else {
      setIsInputValid(false);
      setNewBreed("");
    }
  };
  return (
    <div className="flex mb-4">
      <input
        className="p-2 shadow-inner border rounded-sm outline-none w-full h-full mr-8 focus:bg-blue-100"
        type="text"
        placeholder="Breed ex. German Shepard"
        onKeyUp={(e) => handleInput(e)}
        autoFocus
      />
      <button
        onClick={handleAdd}
        disabled={isBtnDisabled}
        className={`px-4 py-2 bg-blue-500 text-blue-100 rounded-sm 
        focus:outline-none transition duration-300 ${
          isValid && isInputValid ? "hover:bg-blue-700" : "cursor-not-allowed"
        }`}
      >
        Add
      </button>
    </div>
  );
}
