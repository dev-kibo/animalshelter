import React, { useState } from "react";

export default function Remove({
  setIsSelectValid,
  displayVaccines,
  isValid,
  isSelectValid,
  handleRemove,
  setVaccineToRemove,
}) {
  const isBtnValid = useState(isValid && isSelectValid);

  const handleSelect = (e) => {
    if (e.target.value.length > 0) {
      setIsSelectValid(true);
      setVaccineToRemove(e.target.value);
    } else {
      setIsSelectValid(false);
      setVaccineToRemove("");
    }
  };

  return (
    <div className="flex justify-between">
      <select
        onChange={(e) => handleSelect(e)}
        className="focus:outline-none p-2 border border-blue-500 rounded-sm"
      >
        <option value="">Select...</option>
        {displayVaccines}
      </select>
      <button
        onClick={handleRemove}
        disabled={isBtnValid}
        className={`px-4 py-2 border border-red-500 text-red-500 rounded-sm focus:outline-none transition duration-300
        ${
          isValid && isSelectValid
            ? "hover:bg-red-500 hover:text-red-100"
            : "cursor-not-allowed"
        }`}
      >
        Remove
      </button>
    </div>
  );
}
