import React, { useState } from "react";
import axios from "axios";
import Add from "./Add";
import Remove from "./Remove";
import useFetchAllVaccines from "../../../hooks/useFetchAllVaccines";

export default function Vaccine({ isValid, type, setNotif, setNotifColor }) {
  const [isInputValid, setIsInputValid] = useState(false);
  const [isSelectValid, setIsSelectValid] = useState(false);
  const [newVaccine, setNewVaccine] = useState("");
  const [vaccineToRemove, setVaccineToRemove] = useState("");

  const vaccines = useFetchAllVaccines(type);

  const displayVaccines = vaccines.map((b) => (
    <option key={b.id} value={b.id}>
      {b.name}
    </option>
  ));

  const handleAdd = async () => {
    try {
      const data = {
        name: newVaccine,
      };
      if (type === "dog") {
        await axios.post("http://localhost:8080/api/dogs/vaccines", data);
        setNotif("New vaccine added successfully.");
        setNotifColor("green");
      } else if (type === "cat") {
        await axios.post("http://localhost:8081/api/cats/vaccines", data);
        setNotif("New vaccine added successfully.");
        setNotifColor("green");
      }
    } catch (e) {
      setNotif("Adding failed successfully.");
      setNotifColor("yellow");
    }
  };

  const handleRemove = async () => {
    try {
      if (type === "dog") {
        await axios.delete(
          `http://localhost:8080/api/dogs/vaccines/${vaccineToRemove}`
        );
        setNotif("Vaccine removed successfully.");
        setNotifColor("green");
      } else if (type === "cat") {
        await axios.delete(
          `http://localhost:8081/api/cats/vaccines/${vaccineToRemove}`
        );
        setNotif("Vaccine removed successfully.");
        setNotifColor("green");
      }
    } catch (e) {}
  };

  return (
    <div className="flex-1 flex flex-col p-4">
      <p className="text-teal-500 mb-4">Add or remove vaccine</p>
      <Add
        setIsInputValid={setIsInputValid}
        isInputValid={isInputValid}
        isValid={isValid}
        handleAdd={handleAdd}
        setNewVaccine={setNewVaccine}
      />
      <Remove
        displayVaccines={displayVaccines}
        setIsSelectValid={setIsSelectValid}
        isSelectValid={isSelectValid}
        isValid={isValid}
        handleRemove={handleRemove}
        setVaccineToRemove={setVaccineToRemove}
      />
    </div>
  );
}
