import React from "react";

export default function AnimalType({ handleIsValid }) {
  return (
    <div className="flex items-center w-full h-12 mb-8">
      <p className="text-gray-700 mr-4 text-xl">Animal:</p>
      <select
        onChange={(e) => handleIsValid(e)}
        className="focus:outline-none p-2 border border-blue-500 rounded-sm"
      >
        <option value="">Select...</option>
        <option value="cat">Cat</option>
        <option value="dog">Dog</option>
      </select>
    </div>
  );
}
