import React from "react";

export default function BasicInfo({ info }) {
  return (
    <div className="flex-1 flex p-8">
      <div className="flex flex-col text-xl text-gray-500 mr-8">
        <p className="mb-4">Tag:</p>
        <p className="mb-4">Breed:</p>
        <p className="mb-4">Gender:</p>
      </div>
      <div className="flex flex-col text-xl text-gray-700">
        <p className="mb-4">{info.tag}</p>
        <p className="mb-4">{info.breed.name}</p>
        <p className="mb-4 capitalize">{info.gender}</p>
      </div>
    </div>
  );
}
