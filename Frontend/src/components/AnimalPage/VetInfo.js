import React from "react";

export default function VetInfo({ info }) {
  const showVaccines =
    info.vaccines.length > 0 ? (
      info.vaccines.map((vacc) => <li key={vacc.id}>{vacc.name}</li>)
    ) : (
      <p>None</p>
    );

  return (
    <div className="flex-1 flex p-8">
      <div className="flex flex-col text-xl text-gray-500 mr-8">
        <p className="mb-4">Weight:</p>
        <p className="mb-4">Date of birth:</p>
        <p className="mb-4">Previous owner:</p>
        <p className="mb-4">Vaccines:</p>
      </div>
      <div className="flex flex-col text-xl text-gray-700">
        <p className="mb-4">{info.weight} kg</p>
        <p className="mb-4">{info.dateOfBirth || "Not known"}</p>
        <p className="mb-4">{info.previousOwner ? "Yes" : "No"}</p>
        <ul className="divide-y divide-indigo-500 list-disc list-inside">
          {showVaccines}
        </ul>
      </div>
    </div>
  );
}
