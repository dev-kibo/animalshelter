import React from "react";
import { Link, useParams, useLocation } from "react-router-dom";
import useFetchAnimal from "../../hooks/useFetchAnimal";
import BasicInfo from "./BasicInfo";
import VetInfo from "./VetInfo";

export default function AnimalPage() {
  const { id } = useParams();
  const { pathname } = useLocation();

  const info = useFetchAnimal(id, pathname);

  const displayInfo = (
    <>
      <BasicInfo info={info} />
      <VetInfo info={info} />
    </>
  );

  return (
    <div className="w-full lg:p-12 xl:p-24 bg-gray-300">
      <div className="rounded-sm overflow-hidden bg-gray-100 shadow-lg">
        <div className="w-full h-20 bg-teal-500 px-16 flex justify-between items-center">
          <Link
            to="/"
            className="border border-teal-100 py-2 px-4 rounded text-teal-100 transition duration-300 hover:bg-teal-100 hover:text-teal-500"
          >
            Go back
          </Link>
          <p className="text-3xl text-teal-100">Animal Details</p>
        </div>
        <div className="flex py-4 divide-x divide-teal-400">{displayInfo}</div>
      </div>
    </div>
  );
}
