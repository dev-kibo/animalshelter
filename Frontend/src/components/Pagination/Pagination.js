import React from "react";
import ReactPaginate from "react-paginate";

export default function Pagination({ pageCount, results, changePage }) {
  return (
    <div className="flex w-full h-12 my-6 justify-between items-center px-8">
      <p className="text-gray-700">
        Showing <span className="font-bold">{results}</span>{" "}
        {results > 1 ? "results" : "result"}
      </p>
      <ReactPaginate
        pageCount={pageCount}
        pageRangeDisplayed={2}
        marginPagesDisplayed={1}
        containerClassName="flex h-8"
        previousClassName="border border-teal-500 h-full px-2 rounded-sm text-teal-500 transition duration-300
         hover:bg-teal-500 hover:text-teal-100 mr-2"
        previousLinkClassName="focus:outline-none outline-none w-full h-full flex items-center justify-center"
        nextClassName="border border-teal-500 h-full px-2 rounded-sm text-teal-500 transition duration-300
         hover:bg-teal-500 hover:text-teal-100 ml-2"
        nextLinkClassName="focus:outline-none outline-none flex items-center justify-center w-full h-full"
        activeClassName="mx-1 bg-teal-500 w-6 flex items-center justify-center text-center h-full"
        activeLinkClassName="focus:outline-none outline-none text-teal-100"
        pageClassName="mx-1 text-teal-500 border border-teal-500 w-8 h-full rounded-sm
        transition duration-300 hover:bg-teal-500 hover:text-teal-100"
        pageLinkClassName="focust:outline-none outline-none w-full h-full flex items-center justify-center"
        breakClassName="mx-1 h-full w-8 text-teal-500"
        breakLinkClassName="focust:outline-none outline-none w-full h-full flex items-center justify-center"
        onPageChange={changePage}
      />
    </div>
  );
}
