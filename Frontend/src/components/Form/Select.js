import React from "react";

export default function Select({ handle, value, title, options }) {
  return (
    <div className="flex justify-between items-center mb-4 p-2 ">
      <p>{title}</p>
      <select
        onChange={handle}
        value={value}
        className="p-1 outline-none border border-blue-500 rounded-sm"
      >
        <option value="">Select...</option>
        {options.map((o) => (
          <option key={o} value={o}>
            {o.charAt(0).toUpperCase() + o.slice(1)}
          </option>
        ))}
      </select>
    </div>
  );
}
