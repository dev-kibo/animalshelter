import React from "react";

export default function Input({ handle, autofocus, placeholder, value }) {
  return (
    <input
      className="mb-4 p-2 shadow-inner border rounded-sm outline-none focus:bg-blue-100"
      type="text"
      placeholder={placeholder}
      onChange={handle}
      value={value}
      autoFocus={autofocus}
    />
  );
}
