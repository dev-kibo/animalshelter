import React from "react";
import { NavLink } from "react-router-dom";

export default function Sidebar() {
  return (
    <div className="w-1/6 h-full bg-blue-900 flex flex-col flex-shrink-0">
      <div className="flex flex-col w-full p-4 text-blue-300">
        <NavLink
          className="w-full py-2 px-4 mb-2 rounded-sm hover:bg-blue-700 hover:text-blue-100 transition duration-500 ease-in-out"
          activeClassName="bg-blue-700 text-blue-100"
          to="/"
          exact
        >
          Home
        </NavLink>
        <NavLink
          className="w-full py-2 px-4 mb-2 rounded-sm hover:bg-blue-700 hover:text-blue-100 transition duration-500 ease-in-out"
          activeClassName="bg-blue-700 text-blue-100"
          to="/add"
        >
          Add animal
        </NavLink>
        <NavLink
          className="w-full py-2 px-4 mb-2 rounded-sm hover:bg-blue-700 hover:text-blue-100 transition duration-500 ease-in-out"
          activeClassName="bg-blue-700 text-blue-100"
          to="/remove"
        >
          Remove animal
        </NavLink>
        <NavLink
          className="w-full py-2 px-4 mb-2 rounded-sm hover:bg-blue-700 hover:text-blue-100 transition duration-500 ease-in-out"
          activeClassName="bg-blue-700 text-blue-100"
          to="/update"
        >
          Update animal
        </NavLink>
        <NavLink
          className="w-full py-2 px-4 mb-2 rounded-sm hover:bg-blue-700 hover:text-blue-100 transition duration-500 ease-in-out"
          activeClassName="bg-blue-700 text-blue-100"
          to="/manage"
        >
          Manage details
        </NavLink>
      </div>
    </div>
  );
}
