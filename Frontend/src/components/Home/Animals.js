import React from "react";
import Row from "./Row";

export default function Animals({ data, animal }) {
  const displayData = data.map((row) => (
    <Row key={row.id} animal={row} animalType={animal} />
  ));

  const noResults = (
    <div className="flex w-full items-center justify-center my-12">
      <p className="text-2xl text-indigo-400">No results</p>
    </div>
  );

  return (
    <div className="flex flex-col">{data.length ? displayData : noResults}</div>
  );
}
