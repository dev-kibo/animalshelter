import React from "react";
import useFetchAllBreeds from "../../hooks/useFetchAllBreeds";

export default function Filters({
  animal,
  gender,
  breed,
  handleAnimal,
  setBreed,
  setGender,
}) {
  const breeds = useFetchAllBreeds(animal);

  const showBreeds = breeds.map((breed) => (
    <option key={breed.id} value={breed.name}>
      {breed.name}
    </option>
  ));

  return (
    <div className="flex justify-between items-center py-4 px-8">
      <div className="flex items-center">
        <p className="mr-3 font-bold text-gray-600">Show</p>
        <select
          onChange={handleAnimal}
          value={animal}
          className="focus:outline-none border border-blue-500 rounded-sm p-1"
        >
          <option value="cat">Cats</option>
          <option value="dog">Dogs</option>
        </select>
      </div>
      <div className="flex items-center">
        <div className="flex items-center mr-8">
          <p className="mr-3 font-bold text-gray-600">Breed</p>
          <select
            value={breed}
            onChange={(e) => setBreed(e.target.value)}
            className="focus:outline-none border border-blue-500 rounded-sm p-1"
          >
            <option value="all">All</option>
            {showBreeds}
          </select>
        </div>
        <div className="flex items-center">
          <p className="mr-3 font-bold text-gray-600">Gender</p>
          <select
            value={gender}
            onChange={(e) => setGender(e.target.value)}
            className="focus:outline-none border border-blue-500 rounded-sm p-1"
          >
            <option value="all">All</option>
            <option value="male">Male</option>
            <option value="female">Female</option>
          </select>
        </div>
      </div>
    </div>
  );
}
