import React, { useState } from "react";
import SearchBox from "./SearchBox";
import Filters from "./Filters";
import RowHeader from "./RowHeader";
import Pagination from "../Pagination";
import Animals from "./Animals";
import useFetchGender from "../../hooks/useFetchGender";
import useFetchTag from "../../hooks/useFetchTag";
import useFetchAnimals from "../../hooks/useFetchAnimals";
import useFetchBreed from "../../hooks/useFetchBreed";

export default function Home() {
  const [currentPage, setCurrentPage] = useState(1);
  const perPage = 5;
  const [animal, setAnimal] = useState("dog");
  const [gender, setGender] = useState("all");
  const [breed, setBreed] = useState("all");
  const [tag, setTag] = useState("");

  let data;
  const animals = useFetchAnimals(animal);
  const breeds = useFetchBreed(breed, animal);
  const genders = useFetchGender(gender, animal);
  const tagResults = useFetchTag(tag, animal);

  if (breed !== "all") {
    data = breeds;
  } else if (gender !== "all") {
    data = genders;
  } else if (tag.length > 0) {
    data = tagResults;
  } else {
    data = animals;
  }

  const indexOfLastAnimal = perPage * currentPage;
  const indexOfFirstAnimal = indexOfLastAnimal - perPage;
  const currentData = data.slice(indexOfFirstAnimal, indexOfLastAnimal);
  const pageCount = data.length / perPage;

  const changePage = (e) => {
    setCurrentPage(e.selected + 1);
  };

  const handleAnimal = (e) => {
    setAnimal(e.target.value.toLowerCase());
  };
  const showAnimals = (
    <div>
      <Animals data={currentData} animal={animal} />
      <Pagination
        pageCount={pageCount}
        currentPage={currentPage}
        changePage={changePage}
        results={data.length}
      />
    </div>
  );

  return (
    <div className="w-full lg:p-12 xl:p-24 bg-gray-300">
      <div className="rounded-sm overflow-hidden bg-gray-100 shadow-lg">
        <SearchBox setTag={setTag} />
        <Filters
          handleAnimal={handleAnimal}
          setBreed={setBreed}
          setGender={setGender}
          gender={gender}
          breed={breed}
          animal={animal}
        />
        <RowHeader />
        {showAnimals}
      </div>
    </div>
  );
}
