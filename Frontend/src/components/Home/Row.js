import React from "react";
import { Link } from "react-router-dom";

export default function Row({ animal, animalType }) {
  const url = `/animals/${animalType === "dog" ? "dog" : "cat"}/${animal.id}`;

  return (
    <div className="flex justify-around items-center py-4 px-8 border-b border-indigo-500">
      <p className="w-full">{animal.tag}</p>
      <p className="w-full">{animal.breed.name}</p>
      <p className="w-full capitalize">{animal.gender}</p>
      <div className="w-full">
        <Link
          to={url}
          className="border rounded-sm border-blue-500 py-2 px-2 text-blue-500 transition duration-300 ease-in-out hover:bg-blue-500 hover:text-blue-100"
        >
          View More
        </Link>
      </div>
    </div>
  );
}
