import React from "react";

export default function Filter({ setTag }) {
  return (
    <div className="flex bg-teal-400 w-full h-16 flex justify-between items-center px-8">
      <p className="text-2xl text-teal-100">Animals</p>
      <input
        onKeyUp={(e) => setTag(e.target.value)}
        className="focus:bg-teal-100 outline-none rounded-sm h-8 p-3 text-teal-700"
        type="text"
        placeholder="Filter by tag"
      />
    </div>
  );
}
