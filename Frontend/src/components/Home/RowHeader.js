import React from "react";

export default function RowHeader() {
  return (
    <div className="flex justify-around items-center py-4 px-8 border-b-2 border-teal-500">
      <p className="uppercase w-full font-bold text-gray-700">Tag</p>
      <p className="uppercase w-full font-bold text-gray-700">Breed</p>
      <p className="uppercase w-full font-bold text-gray-700">Gender</p>
      <p className="uppercase w-full font-bold text-gray-700">Action</p>
    </div>
  );
}
