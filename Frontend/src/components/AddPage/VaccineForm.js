import React, { useState, useEffect } from "react";
import Vaccine from "./Vaccine";
import useFetchAllVaccines from "../../hooks/useFetchAllVaccines";

export default function VaccineForm({ setVaccines, vaccines, type }) {
  const [currentData, setCurrentData] = useState([]);

  const data = useFetchAllVaccines(type);

  useEffect(() => {
    setCurrentData(data);
  }, [data]);

  const filter = (e) => {
    const filteredData = data.filter((v) =>
      v.name.toLowerCase().includes(e.target.value.toLowerCase())
    );
    setCurrentData(filteredData);
  };

  return (
    <div className="p-8 pb-0 flex-1 text-gray-700">
      <div className="flex flex-col">
        <div className="flex justify-between items-center mb-4 h-8">
          <p className="font-bold text-gray-500">Select vaccines</p>
          <input
            className="p-2 shadow-inner border rounded-sm outline-none focus:bg-blue-100 h-full text-sm"
            type="text"
            placeholder="Filter by name"
            onKeyUp={filter}
          />
        </div>
        <div className="flex flex-col rounded-sm h-80 overflow-y-auto p-2">
          {currentData.map((v) => (
            <Vaccine
              setVaccines={setVaccines}
              vaccines={vaccines}
              key={v.id}
              v={v}
            />
          ))}
        </div>
      </div>
    </div>
  );
}
