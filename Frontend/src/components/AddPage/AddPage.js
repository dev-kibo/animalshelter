import React, { useState, useEffect } from "react";
import Notification from "../Notification";
import DetailsForm from "./DetailsForm";
import VaccineForm from "./VaccineForm";
import Actions from "./Actions";
import axios from "axios";

export default function AddPage() {
  const [tag, setTag] = useState("");
  const [weight, setWeight] = useState("");
  const [dob, setDob] = useState("");
  const [type, setType] = useState("");
  const [hadOwner, setHadOwner] = useState("");
  const [gender, setGender] = useState("");
  const [breed, setBreed] = useState("");
  const [vaccines, setVaccines] = useState([]);
  const [isFormValid, setIsFormValid] = useState(false);
  const [notif, setNotif] = useState("");
  const [notifColor, setNotifColor] = useState("");

  const handleSubmit = async () => {
    if (tag && weight && type && hadOwner && gender && breed) {
      const data = {
        tag,
        weight,
        dateOfBirth: dob,
        previousOwner: hadOwner.toLowerCase() === "yes" ? true : false,
        gender,
        breed,
        vaccines,
      };
      if (type === "dog") {
        try {
          await axios.post("http://localhost:8080/api/dogs", data);
          setNotif("Animal added successfully");
          setNotifColor("blue");
        } catch (e) {
          if (e.response.status === 400) {
            setNotif("Tag already exists.");
            setNotifColor("yellow");
          }
        }
      } else if (type === "cat") {
        try {
          await axios.post("http://localhost:8081/api/cats", data);
          setNotif("Animal added successfully");
          setNotifColor("blue");
        } catch (e) {
          if (e.response.status === 400) {
            setNotif("Tag already exists.");
            setNotifColor("yellow");
          }
        }
      }
    }
  };

  useEffect(() => {
    if (tag && weight && dob && type && hadOwner && gender && breed) {
      setIsFormValid(true);
    }
  }, [tag, weight, dob, type, hadOwner, gender, breed]);

  return (
    <div className="w-full lg:p-12 xl:p-24 bg-gray-300">
      {notif ? <Notification text={notif} color={notifColor} /> : ""}
      <div className="flex rounded-sm overflow-hidden pt-4 bg-gray-100 shadow-lg">
        <DetailsForm
          setTag={setTag}
          setWeight={setWeight}
          setDob={setDob}
          setType={setType}
          setHadOwner={setHadOwner}
          setGender={setGender}
          setBreed={setBreed}
          setNotif={setNotif}
          setNotifColor={setNotifColor}
          type={type}
          hadOwner={hadOwner}
          gender={gender}
          breed={breed}
        />
        <VaccineForm
          setVaccines={setVaccines}
          vaccines={vaccines}
          type={type}
        />
      </div>
      <Actions handleSubmit={handleSubmit} isFormValid={isFormValid} />
    </div>
  );
}
