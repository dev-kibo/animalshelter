import React from "react";
import DialogActions from "./DialogActions";

export default function Dialog({ data, setIsFound, doRemove }) {
  return (
    <div className="absolute top-0 left-0 w-full h-full flex justify-center items-center bg-gray-700 bg-opacity-75">
      <div className="bg-gray-100 rounded-sm flex flex-col p-6 text-gray-700 w-1/3">
        <p className="text-2xl font-bold text-gray-600 text-center">
          Are you sure?
        </p>
        <p className="mt-4 text-red-400">
          The following animal is going to be removed
        </p>
        <div className="flex text-xl mt-4">
          <div className="mr-2">
            <p className="text-gray-600">Tag:</p>
            <p className="text-gray-600">Breed:</p>
            <p className="text-gray-600">Gender</p>
          </div>
          <div>
            <p>{data.tag}</p>
            <p>{data.breed.name}</p>
            <p className="capitalize">{data.gender}</p>
          </div>
        </div>
        <DialogActions doRemove={doRemove} setIsFound={setIsFound} />
      </div>
    </div>
  );
}
