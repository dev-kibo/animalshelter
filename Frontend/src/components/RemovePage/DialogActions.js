import React from "react";

export default function DialogActions({ setIsFound, doRemove }) {
  return (
    <div className="flex justify-end items-center mt-4">
      <button
        onClick={() => setIsFound(0)}
        className="rounded-sm border border-gray-500 text-gray-500 py-2 px-4 mr-2 transition duration-300 hover:bg-gray-300 hover:text-gray-700 focus:outline-none"
      >
        Cancel
      </button>
      <button
        onClick={doRemove}
        className="rounded-sm border bg-red-600 text-red-100 py-2 px-4 transition duration-300 hover:bg-red-700 focus:outline-none"
      >
        Confirm
      </button>
    </div>
  );
}
