import React, { useState } from "react";
import SearchBox from "./SearchBox";
import Dialog from "./Dialog";
import Notification from "../Notification";
import axios from "axios";

export default function RemovePage() {
  const [isFound, setIsFound] = useState(0);
  const [search, setSearch] = useState("");
  const [data, setData] = useState({});

  const doSearch = async () => {
    try {
      let res;
      if (search.startsWith("D-")) {
        res = await axios.get(
          `http://localhost:8080/api/dogs/filter/tag/${search}`
        );
        setData(res.data[0]);
        setIsFound(2);
      } else if (search.startsWith("C-")) {
        res = await axios.get(
          `http://localhost:8081/api/cats/filter/tag/${search}`
        );
        setData(res.data[0]);
        setIsFound(2);
      } else {
        setIsFound(1);
      }
    } catch (e) {
      // 404
      setIsFound(1);
    }
  };

  const doRemove = async () => {
    try {
      const id = data.id;
      if (data.tag.startsWith("D")) {
        await axios.delete(`http://localhost:8080/api/dogs/${id}`);
      } else if (data.tag.startsWith("C")) {
        await axios.delete(`http://localhost:8081/api/cats/${id}`);
      }
      setIsFound(3);
      setSearch("");
    } catch (e) {
      // delete error
    }
  };

  return (
    <div className="relative w-full lg:p-12 xl:p-24 bg-gray-300">
      {isFound === 3 ? (
        <Notification text="Animal successfully removed" color="blue" />
      ) : (
        ""
      )}
      {isFound === 1 ? (
        <Notification text="Animal not found" color="red" />
      ) : (
        ""
      )}
      <SearchBox
        setSearch={setSearch}
        doSearch={doSearch}
        setIsFound={setIsFound}
        search={search}
      />
      {isFound === 2 ? (
        <Dialog doRemove={doRemove} data={data} setIsFound={setIsFound} />
      ) : (
        ""
      )}
    </div>
  );
}
