import React, { useState } from "react";

export default function SearchBox({ doSearch, setSearch, setIsFound, search }) {
  const [disabled, setDisabled] = useState(true);

  const handleSearch = (e) => {
    if (e.target.value.length > 0) {
      setDisabled(false);
    } else {
      setDisabled(true);
      setIsFound(0);
    }
    setSearch(e.target.value);
  };

  return (
    <div className="text-gray-700 border-sm py-4 px-24 bg-gray-100 shadow-lg flex justify-between">
      <input
        className="p-2 shadow-inner border rounded-sm outline-none focus:bg-blue-100 w-full mr-8"
        type="text"
        placeholder="Tag ex. D-4342 or C-A4212"
        onChange={(e) => handleSearch(e)}
        autoFocus
        value={search}
      />
      <button
        onClick={doSearch}
        disabled={disabled}
        className={`${
          disabled
            ? "cursor-not-allowed"
            : "hover:bg-red-500 hover:text-red-100"
        } rounded-sm border border-red-500 text-red-400 py-1 px-3 transition duration-300  focus:outline-none`}
      >
        Remove
      </button>
    </div>
  );
}
