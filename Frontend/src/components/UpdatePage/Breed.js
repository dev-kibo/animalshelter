import React from "react";
import useFetchAllBreeds from "../../hooks/useFetchAllBreeds";

export default function Breed({ breed, handleBreed, type }) {
  const data = useFetchAllBreeds(type);

  const displayBreeds = data.map((b) => (
    <option key={b.id} value={b.id}>
      {b.name}
    </option>
  ));

  return (
    <div className="flex justify-between items-center mb-4 p-2">
      <p>Breed</p>
      <select
        value={breed}
        onChange={handleBreed}
        className="p-1 outline-none border border-blue-500 rounded-sm"
      >
        <option value="">Select...</option>
        {displayBreeds}
      </select>
    </div>
  );
}
