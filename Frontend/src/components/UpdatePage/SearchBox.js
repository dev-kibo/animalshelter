import React, { useState } from "react";

export default function SearchBox({ search, setSearch, doSearch, setNotif }) {
  const [isDisabled, setIsDisabled] = useState(true);

  const handleSearch = (e) => {
    const val = e.target.value;
    if (val.length > 0) {
      setIsDisabled(false);
    } else {
      setIsDisabled(true);
      setNotif("");
    }
    setSearch(val);
  };

  return (
    <div className="w-full flex items-center px-24 py-4 rounded-sm overflow-hidden bg-gray-100 shadow-lg mb-3">
      <input
        className="p-2 shadow-inner border rounded-sm outline-none w-full focus:bg-blue-100 mr-8"
        type="text"
        onChange={(e) => handleSearch(e)}
        value={search}
        autoFocus
        placeholder="Find animal by tag ex. D-43414"
      />
      <button
        onClick={doSearch}
        disabled={isDisabled}
        className={`focus:outline-none rounded-sm border border-teal-500 text-teal-500 py-2 px-4 transition duration-300 
        ${
          isDisabled
            ? "cursor-not-allowed"
            : "hover:bg-teal-500 hover:text-teal-100"
        }`}
      >
        Find
      </button>
    </div>
  );
}
