import React from "react";

export default function Actions({ handleSubmit, isFormValid }) {
  return (
    <div className="flex justify-end bg-gray-100 w-full pb-4 pr-8 shadow-lg">
      <button
        onClick={handleSubmit}
        disabled={!isFormValid}
        className={`${
          isFormValid
            ? "bg-teal-500 hover:bg-teal-900 border-teal-500 "
            : "cursor-not-allowed bg-teal-300 border-teal-300"
        }  text-teal-100 px-4 py-2 rounded-sm border
          focus:outline-none transition duration-300`}
      >
        Save
      </button>
    </div>
  );
}
