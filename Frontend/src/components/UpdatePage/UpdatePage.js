import React, { useState, useEffect } from "react";
import Notification from "../Notification";
import DetailsForm from "./DetailsForm";
import VaccineForm from "./VaccineForm";
import Actions from "./Actions";
import axios from "axios";
import SearchBox from "./SearchBox";

export default function UpdatePage() {
  const [id, setId] = useState();
  const [weight, setWeight] = useState("");
  const [dob, setDob] = useState("");
  const [type, setType] = useState("");
  const [hadOwner, setHadOwner] = useState("");
  const [gender, setGender] = useState("");
  const [breed, setBreed] = useState("");
  const [vaccines, setVaccines] = useState([]);
  const [isFormValid, setIsFormValid] = useState(false);
  const [notif, setNotif] = useState("");
  const [notifColor, setNotifColor] = useState("");
  const [isFound, setIsFound] = useState(0);
  const [search, setSearch] = useState("");

  const doSearch = async () => {
    try {
      let res;
      if (search.startsWith("D-")) {
        res = await axios.get(
          `http://localhost:8080/api/dogs/filter/tag/${search}`
        );

        setId(res.data[0].id);
        setWeight(res.data[0].weight);
        setDob(res.data[0].dateOfBirth);
        setHadOwner(res.data[0].previousOwner ? "yes" : "no");
        setGender(res.data[0].gender);
        setBreed(res.data[0].breed.id);
        setVaccines(res.data[0].vaccines.map((v) => v.id));
        setType("dog");

        setIsFound(2);
      } else if (search.startsWith("C-")) {
        res = await axios.get(
          `http://localhost:8081/api/cats/filter/tag/${search}`
        );

        setId(res.data[0].id);
        setWeight(res.data[0].weight);
        setDob(res.data[0].dateOfBirth);
        setHadOwner(res.data[0].previousOwner ? "yes" : "no");
        setGender(res.data[0].gender);
        setBreed(res.data[0].breed.id);
        setVaccines(res.data[0].vaccines.map((v) => v.id));
        setType("cat");

        setIsFound(2);
      } else {
        setIsFound(1);
      }
    } catch (e) {
      // 404
      setIsFound(1);
      setNotifColor("red");
      setNotif("Animal not found.");
    }
  };
  const handleSubmit = async () => {
    if (weight && dob && type && hadOwner && gender && breed) {
      const data = {
        weight,
        dateOfBirth: dob,
        previousOwner: hadOwner.toLowerCase() === "yes" ? true : false,
        gender,
        breed,
        vaccines,
      };
      if (type === "dog") {
        try {
          await axios.put(`http://localhost:8080/api/dogs/${id}`, data);
          setNotif("Animal updated successfully");
          setNotifColor("green");
        } catch (e) {}
      } else {
        try {
          await axios.put(`http://localhost:8081/api/cats/${id}`, data);
          setNotif("Animal updated successfully");
          setNotifColor("green");
        } catch (e) {}
      }
    }
  };

  const displayForm = (
    <>
      <div className="flex rounded-sm overflow-hidden pt-4 bg-gray-100 shadow-lg">
        <DetailsForm
          setWeight={setWeight}
          setDob={setDob}
          setHadOwner={setHadOwner}
          setGender={setGender}
          setBreed={setBreed}
          setNotif={setNotif}
          setNotifColor={setNotifColor}
          type={type}
          hadOwner={hadOwner}
          gender={gender}
          breed={breed}
          weight={weight}
          dob={dob}
        />
        <VaccineForm
          setVaccines={setVaccines}
          type={type}
          vaccines={vaccines}
        />
      </div>
      <Actions handleSubmit={handleSubmit} isFormValid={isFormValid} />
    </>
  );

  useEffect(() => {
    if (weight && dob && hadOwner && gender && breed) {
      setIsFormValid(true);
    }
  }, [weight, dob, hadOwner, gender, breed]);

  return (
    <div className="w-full lg:p-12 xl:p-24 bg-gray-300">
      {notif ? <Notification text={notif} color={notifColor} /> : ""}
      <SearchBox
        setSearch={setSearch}
        doSearch={doSearch}
        setNotif={setNotif}
        search={search}
      />
      {isFound === 2 ? displayForm : ""}
    </div>
  );
}
