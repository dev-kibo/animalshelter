import React from "react";
import Breed from "./Breed";
import Input from "../Form/Input";
import Select from "../Form/Select";

export default function DetailsForm({
  setWeight,
  setDob,
  setHadOwner,
  setGender,
  setBreed,
  setNotif,
  setNotifColor,
  breed,
  hadOwner,
  gender,
  dob,
  weight,
  type,
}) {
  const handleWeight = (e) => {
    const val = e.target.value;

    if (val.length > 0) {
      if (/^[\d, \\.]+$/.test(val)) {
        setNotif("");
        setNotifColor("");
        setWeight(Number.parseFloat(val));
      } else {
        setNotif("Weight can only contain numbers.");
        setNotifColor("red");
        setWeight("");
      }
    } else {
      setNotif("");
      setNotifColor("");
      setWeight("");
    }
  };

  const handleDob = (e) => {
    const val = e.target.value;

    if (val.length > 0) {
      if (/^[\d]{1,2}\/[\d]{1,2}\/[\d]{4}$/.test(val)) {
        setNotif("");
        setNotifColor("");
        setDob(val);
      } else {
        setNotif("Please provide a valid date format.");
        setNotifColor("red");
        setDob("");
      }
    } else {
      setNotif("");
      setNotifColor("");
      setDob("");
    }
  };

  const handleHadOwner = (e) => {
    setHadOwner(e.target.value);
  };
  const handleGender = (e) => {
    setGender(e.target.value);
  };
  const handleBreed = (e) => {
    setBreed(Number.parseInt(e.target.value));
  };

  return (
    <div className="p-8 pb-0 border-r-2 border-blue-500 flex-1 flex flex-col text-gray-700">
      <Input
        handle={handleWeight}
        placeholder="Weight in kg ex. 2.45"
        value={weight}
      />
      <Input
        handle={handleDob}
        placeholder="Date of birth ex. dd/mm/yyyy"
        value={dob}
      />
      <Select
        title="Had previous owner?"
        value={hadOwner}
        handle={handleHadOwner}
        options={["yes", "no"]}
      />
      <Select
        title="Gender"
        value={gender}
        handle={handleGender}
        options={["male", "female"]}
      />
      <Breed breed={breed} type={type} handleBreed={handleBreed} />
    </div>
  );
}
