import React from "react";

export default function Vaccine({ v, setVaccines, vaccines, checked }) {
  const handleVaccines = (e) => {
    const id = Number.parseInt(e.target.value);
    const isFound = vaccines.find((v) => v === id);
    if (isFound) {
      setVaccines(vaccines.filter((v) => v !== id));
    } else if (!isFound) {
      setVaccines([...vaccines, id]);
    }
  };
  return (
    <div className="flex justify-between items-center py-2 px-4 border-b border-blue-500">
      <p>{v.name}</p>
      <input
        value={v.id}
        onChange={(e) => handleVaccines(e)}
        type="checkbox"
        className="w-5 h-5"
        checked={checked || ""}
      />
    </div>
  );
}
