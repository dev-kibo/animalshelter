## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)

## General info
This project is full stack web application that uses two REST APIs and React frontend to manage shelter animals with CRUD operations.  
  
![Project image](images/springBoot.png)

## Technologies
Project is created with:
* [Spring Boot](https://spring.io/projects/spring-boot)
* [React](https://reactjs.org)
* [MySql](https://www.mysql.com)
* [tailwindcss](https://tailwindcss.com)
	
## Setup
To run this project you will need:
* [MySql Server](https://dev.mysql.com/downloads/mysql/)
* [Gradle](https://gradle.org/) or [Maven](https://maven.apache.org/)

If using Gradle start it from project directory with command:
```
$ ./gradlew bootRun
```
Or with maven:
```
$ ./mvnw spring-boot:run
```

Alternately you could use IDE of your choice.